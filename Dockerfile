FROM php:7.4-apache
RUN docker-php-ext-install mysqli && docker-php-ext-install pdo_mysql
COPY ./project-folder/* /var/www/html/
COPY ./project-folder/image/* /var/www/html/image/
